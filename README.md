# Font recognition

Utility that tries to recognize the font used in an image (logos, etc.).

## Usage

```sh
whatfont <image> <font>
```

To get a list of installed fonts on linux-based systems (using fontconfig):
```sh
fc-list --format="%{file}\n"
```

## Requirements

### Linux

On linux, it requires a development version of both libpng and freetype2 installed.

### Windows

(WIP)

