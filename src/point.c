#include "point.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

void ptqueue_init(ptqueue_t *q)
{
	q->buf = NULL;
	q->size = 0;
	q->capacity = 0;
}

bool ptqueue_empty(ptqueue_t *q)
{
	return q->size == 0;
}

point_t ptqueue_take(ptqueue_t *q)
{
	if (q->size == 0) abort();
	point_t pt = q->buf[0];
	q->size--;
	if (q->size > 0)
		memmove(q->buf, q->buf + 1, q->size * sizeof(point_t));
	return pt;
}

void ptqueue_push(ptqueue_t *q, point_t p)
{
	if (q->buf == NULL)
	{
		q->capacity = 1;
		q->buf = malloc(q->capacity * sizeof(point_t));
	}
	else if (q->size == q->capacity)
	{
		q->capacity *= 2;
		q->buf = realloc(q->buf, q->capacity * sizeof(point_t));
	}

	q->buf[q->size] = p;
	q->size++;
}

void ptqueue_free(ptqueue_t *q)
{
	if (q->buf)
		free(q->buf);
	q->buf = NULL;
	q->size = 0;
	q->capacity = 0;
}
