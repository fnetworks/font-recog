#ifndef IMAGE_LIST_H
#define IMAGE_LIST_H

#include "image.h"
#include <stddef.h>

typedef struct {
	image_t **buf;
	size_t size;
	size_t capacity;
} imglist_t, *imglist;

imglist new_imglist();
void imglist_destroy(imglist *list);

void imglist_reserve(imglist list, size_t capacity);

void imglist_add(imglist list, image_t *image);

image_t **imglist_items(imglist list);

image_t *imglist_at(imglist list, size_t index);

#endif
