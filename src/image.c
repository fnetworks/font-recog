#include "image.h"

#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>

void destroy_image(image_t *image)
{
	if (image->data)
	{
		free(image->data);
		image->data = NULL;
		image->height = 0;
	}
	free(image);
}

// TODO error handling
image_t *new_image(int width, int height) {
	image_t *img = (image_t*) malloc(sizeof(image_t));
	if (!img) abort();
	img->row_bytes = width;
	img->width = width;
	img->height = height;
	img->bit_depth = 8;
	img->color_type = 0;
	img->data = malloc(sizeof(uint8_t) * width * height);
	if (!img->data) abort();
	return img;
}

uint8_t image_get(image_t *img, int x, int y) {
	// TODO
	if (x < 0 || x >= img->width || y < 0 || y >= img->height) return 0xFF;
	return img->data[y * img->row_bytes + x];
}

void image_set(image_t *img, int x, int y, uint8_t color) {
	if (x < 0 || x >= img->width || y < 0 || y >= img->height) return;
	img->data[y * img->row_bytes + x] = color;
}

void clear_image(image_t *img, uint8_t color) {
	memset(img->data, color, img->row_bytes * img->height);
}

image_t *subimage(image_t *src, int x1, int y1, int x2, int y2) {
	if (x1 >= x2 || y1 >= y2) abort();
	int width = x2 - x1;
	int height = y2 - y1;
	image_t *dst = new_image(width, height);
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			uint8_t c = image_get(src, x1 + x, y1 + y);
			image_set(dst, x, y, c);
		}
	}
	return dst;
}

void resize_height(image_t *img, int height) {
	if (img->height == height) return;
	if (height == 0) abort();
	
	int width = (int)round((float) height / (float) img->height * (float) img->width);

	uint8_t *res = (uint8_t*) malloc(sizeof(uint8_t) * width * height);
	assert(res);
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			int ox = (int)round((float) x / (float) width * (float) img->width);
			int oy = (int)round((float) y / (float) height * (float) img->height);
			res[y * width + x] = img->data[oy * img->width + ox];
		}
	}

	img->width = width;
	img->row_bytes = width;
	img->height = height;
	free(img->data);
	img->data = res;
}
