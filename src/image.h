#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

typedef struct _image_t {
	int      width, height;
	uint32_t row_bytes;
	uint8_t  color_type, bit_depth;
	uint8_t  *data;
} image_t;

image_t *new_image(int width, int height);
void destroy_image(image_t *image);

uint8_t image_get(image_t *image, int x, int y);
void image_set(image_t *image, int x, int y, uint8_t color);

void clear_image(image_t *image, uint8_t color);
//void clear_image_region(image_t *image, uint8_t color, int x, int y, int width, int height);
image_t *subimage(image_t *src, int x1, int y1, int x2, int y2);

void resize_height(image_t *image, int height);

image_t *read_png(char *filename);
void write_png(char *filename, image_t *image);

#endif
