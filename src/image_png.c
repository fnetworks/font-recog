#include "image.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <png.h>

image_t *read_png(char *filename) {
	FILE *fp = fopen(filename, "rb");
	if (!fp)
	{
		fprintf(stderr, "Could not open image %s: %s", filename, strerror(errno));
		abort();
	}

	unsigned char header[8];
	if (fread(header, 1, 8, fp) < 8)
	{
		fprintf(stderr, "Could not read PNG header\n");
		abort();
	}
	if (png_sig_cmp(header, 0, 8) != 0) {
		fprintf(stderr, "%s is not a png file\n", filename);
		abort();
	}

	png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		fprintf(stderr, "Failed to create PNG read struct\n");
		abort();
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		fprintf(stderr, "Failed to create PNG info struct\n");
		abort();
	}

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "Error during PNG init_io\n");
		abort();
	}

	png_init_io(png_ptr, fp);
	png_set_sig_bytes(png_ptr, 8);

	png_read_info(png_ptr, info_ptr);

	image_t *img = (image_t*) malloc(sizeof(image_t));
	if (!img)
	{
		fprintf(stderr, "Could not allocate image pointer\n");
		abort();
	}

	img->width = png_get_image_width(png_ptr, info_ptr);
	img->height = png_get_image_height(png_ptr, info_ptr);
	img->color_type = png_get_color_type(png_ptr, info_ptr);
	img->bit_depth = png_get_bit_depth(png_ptr, info_ptr);

	if (img->color_type == PNG_COLOR_TYPE_PALETTE)
		png_set_expand(png_ptr);
	if (img->color_type == PNG_COLOR_TYPE_GRAY && img->bit_depth < 8)
		png_set_expand(png_ptr);
	if (img->bit_depth == 16)
		png_set_strip_16(png_ptr);
	if (img->color_type & PNG_COLOR_MASK_ALPHA)
		png_set_strip_alpha(png_ptr);

	png_set_interlace_handling(png_ptr);
	png_read_update_info(png_ptr, info_ptr);

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "Error during PNG read_image setup\n");
		free(img);
		abort();
	}

	img->row_bytes = png_get_rowbytes(png_ptr, info_ptr);
	img->data = (png_bytep) malloc(sizeof(png_byte) * img->height * img->row_bytes);
	if (!img->data)
	{
		fprintf(stderr, "Failed to allocate memory for image data\n");
		free(img);
		abort();
	}

	png_bytep *row_pointers = malloc(sizeof(png_bytep) * img->height);
	if (!row_pointers)
	{
		fprintf(stderr, "Failed to allocate row_pointers\n");
		free(img->data);
		free(img);
		abort();
	}
	for (int y = 0; y < img->height; y++)
		row_pointers[y] = img->data + y * img->row_bytes;

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "Error during PNG read_image\n");
		free(row_pointers);
		free(img->data);
		free(img);
		abort();
	}

	png_read_image(png_ptr, row_pointers);
	free(row_pointers);
	png_read_end(png_ptr, NULL);

	img->color_type = png_get_color_type(png_ptr, info_ptr);

	if (png_ptr && info_ptr)
		png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

	fclose(fp);

	return img;
}

void write_png(char *filename, image_t *img) {
	FILE *fp = fopen(filename, "wb");
	if (!fp)
	{
		fprintf(stderr, "Could not open image %s: %s", filename, strerror(errno));
		abort();
	}

	png_structp png_ptr;
	png_infop info_ptr;

	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if (!png_ptr)
	{
		fprintf(stderr, "Failed to create png ptr\n");
		abort();
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr)
	{
		fprintf(stderr, "Failed to create info ptr\n");
		png_destroy_write_struct(&png_ptr, NULL);
		abort();
	}

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		fprintf(stderr, "png writing failed\n");
		png_destroy_write_struct(&png_ptr, &info_ptr);
		abort();
	}

	png_init_io(png_ptr, fp);

	png_set_IHDR(png_ptr, info_ptr,
			img->width, img->height,
			img->bit_depth, img->color_type,
			PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

	png_write_info(png_ptr, info_ptr);
	png_set_packing(png_ptr);

	for (int y = 0; y < img->height; y++)
		png_write_row(png_ptr, img->data + y * img->row_bytes);
	png_write_end(png_ptr, NULL);

	if (png_ptr && info_ptr)
		png_destroy_write_struct(&png_ptr, &info_ptr);
	fclose(fp);
}
