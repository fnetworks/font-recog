#include "image_list.h"

#include <stdlib.h>

void imglist_init(imglist list) {
	list->buf = NULL;
	list->size = 0;
	list->capacity = 0;
}

imglist new_imglist() {
	imglist list = (imglist) malloc(sizeof(imglist_t));
	imglist_init(list);
	return list;
}

void imglist_free(imglist list) {
	for (size_t i = 0; i < list->size; i++)
		destroy_image(list->buf[i]);
	list->size = 0;

	free(list->buf);
	list->buf = NULL;
	list->capacity = 0;
}

void imglist_destroy(imglist *list) {
	imglist_free(*list);
	free(*list);
	*list = NULL;
}


void imglist_reserve(imglist list, size_t capacity) {
	if (list->capacity > capacity) return;

	list->capacity = capacity;

	if (list->buf == NULL)
		list->buf = malloc(list->capacity * sizeof(image_t*));
	else
		list->buf = realloc(list->buf, list->capacity * sizeof(image_t*));
	if (!list->buf) abort();
}

void imglist_add(imglist list, image_t *image)
{
	if (list->buf == NULL)
	{
		list->capacity = 1;
		list->buf = malloc(list->capacity * sizeof(image_t*));
	}
	else if (list->size == list->capacity)
	{
		list->capacity *= 2;
		list->buf = realloc(list->buf, list->capacity * sizeof(image_t*));
	}

	list->buf[list->size] = image;
	list->size++;
}

image_t **imglist_items(imglist list)
{
	return list->buf;
}

image_t *imglist_at(imglist list, size_t index)
{
	if (index >= list->size) abort();
	return list->buf[index];
}
