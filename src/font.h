#ifndef FONT_H
#define FONT_H

#include "image_list.h"

void font_init();
void font_shutdown();

imglist font_load(const char *file);

#endif
