#ifndef POINT_H
#define POINT_H

#include <stdbool.h>
#include <stddef.h>

typedef struct {
	int x;
	int y;
} point_t;

typedef struct {
	point_t *buf;
	size_t size;
	size_t capacity;
} ptqueue_t;

void ptqueue_init(ptqueue_t *q);
bool ptqueue_empty(ptqueue_t *q);
point_t ptqueue_take(ptqueue_t *q);
void ptqueue_push(ptqueue_t *q, point_t p);
void ptqueue_free(ptqueue_t *q);

#endif
