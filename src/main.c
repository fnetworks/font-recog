#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <string.h>
#include <assert.h>
#include <math.h>

#include <png.h>

#include "image.h"
#include "image_list.h"
#include "point.h"
#include "font.h"

void make_gray(image_t *img)
{
	if (img->color_type & PNG_COLOR_MASK_COLOR) {
		uint8_t *new_data = (uint8_t*) malloc(sizeof(uint8_t) * img->height * img->width);
		assert(img->row_bytes / img->width == 3);
		for (int y = 0; y < img->height; y++) {
			for (int x = 0; x < img->width; x++) {
				float r = img->data[y * img->row_bytes + x * 3 + 0];
				float g = img->data[y * img->row_bytes + x * 3 + 1];
				float b = img->data[y * img->row_bytes + x * 3 + 2];
				new_data[y * img->width + x] = (uint8_t) round(0.2126 * r + 0.7152 * g + 0.0722 * b);
			}
		}

		free(img->data);
		img->data = new_data;
		img->row_bytes = img->width;
		img->bit_depth = 8;
		img->color_type = PNG_COLOR_TYPE_GRAY;
	}
}

float compare(image_t *a, image_t *b) {
	int width = a->width < b->width ? a->width : b->width;
	int height = a->height < b->height ? a->height : b->height;
	float result = 0;
	for (int y = 0; y < height; y++) {
		for (int x = 0; x < width; x++) {
			uint8_t av = a->data[y * a->row_bytes + x];
			uint8_t bv = b->data[y * b->row_bytes + x];

			uint8_t diff_inv = 255 - (av > bv ? av - bv : bv - av);
			result += diff_inv;
		}
	}
	result /= width * height;
	return  result / 255.0f;
}

uint8_t get_background(image_t *img) {
	/*uint8_t samples[4];
	samples[0] = img->data[0];
	samples[1] = img->data[img->row_bytes];
	samples[2] = img->data[img->height * img->row_bytes];
	samples[3] = img->data[img->height * img->row_bytes + img->row_bytes - 1];
	if (samples[0] == samples[1] && samples[1] == samples[2] && samples[2] == samples[3]) {
		return samples[0];
	} else {
		fprintf(stderr, "Sample error\n");
		abort();
	}*/
	(void) img;
	return 0xFF; // TODO
}

void pt_null(point_t *p) {
	p->x = -1;
	p->y = -1;
}

bool is_pt_null(point_t *p) {
	return p->x == -1;
}


typedef enum {
	DIR_LEFT = 0,
	DIR_UP = 1,
	DIR_RIGHT = 2,
	DIR_DOWN = 3,
} dir_t;

dir_t dir_rotr(dir_t base, uint8_t rotate) {
	dir_t rot = base + rotate;
	return rot % 4;
}

dir_t dir_rotl(dir_t base, uint8_t rotate) {
	dir_t rot = base - rotate;
	return rot % 4;
}

point_t pt_front(point_t p, dir_t dir) {
	point_t ret = p;
	switch (dir) {
		case DIR_LEFT:
			ret.x -= 1;
			break;
		case DIR_RIGHT:
			ret.x += 1;
			break;
		case DIR_UP:
			ret.y += 1;
			break;
		case DIR_DOWN:
			ret.y -= 1;
			break;
	}
	return ret;
}

int pt_front_x(point_t p, dir_t dir) {
	if (dir == DIR_LEFT)
		return p.x - 1;
	else if (dir == DIR_RIGHT)
		return p.x + 1;
	else
		return p.x;
}

int pt_front_y(point_t p, dir_t dir) {
	if (dir == DIR_UP)
		return p.y - 1;
	else if (dir == DIR_DOWN)
		return p.y + 1;
	else
		return p.y;
}

#define BLACK 0x00
#define WHITE 0xFF

#define BACKGROUND_TRESHOLD 10

imglist find_letters(image_t *img)
{
	uint8_t bg = get_background(img);

	/*for (int i = 0; i < img->width * img->height; i++) {
		uint8_t color = img->data[i];
		uint8_t bg_dist = bg > color ? bg - color : color - bg;
		if (bg_dist > BACKGROUND_TRESHOLD)
			img->data[i] = BLACK;
		else
			img->data[i] = WHITE;
	}*/

	image_t *canvas = new_image(img->width, img->height);

	imglist list = new_imglist();

	point_t point;
	int x = 0, y = 0;
	while (true) {
		for (; y < img->height; y++) {
			for (; x < img->width; x++) {
				uint8_t color = image_get(img, x, y);
				uint8_t bgdist = bg > color ? bg - color : color - bg;
				if (bgdist > BACKGROUND_TRESHOLD) {
					point.x = x;
					point.y = y;
					goto found;
				}
			}
			x = 0;
		}
		break;

found:
		{
			clear_image(canvas, WHITE);

			point_t min = {.x = img->width, .y = img->height};
			point_t max = {.x = 0, .y = 0};

			// See https://en.wikipedia.org/wiki/Flood_fill#Alternative_implementations
			image_set(img, point.x, point.y, WHITE);
			image_set(canvas, point.x, point.y, BLACK);

#define pt_min(dst, pt) \
			if (pt.x < dst.x) dst.x = pt.x; \
			if (pt.y < dst.y) dst.y = pt.y;
#define pt_max(dst, pt) \
			if (pt.x > dst.x) dst.x = pt.x; \
			if (pt.y > dst.y) dst.y = pt.y;

			ptqueue_t Q;
			ptqueue_init(&Q);

			ptqueue_push(&Q, point);

			while (!ptqueue_empty(&Q)) {
				point_t n = ptqueue_take(&Q);
				pt_min(min, n);
				pt_max(max, n);
				for (uint8_t i = 0; i < 4; i++) {
					point_t front = pt_front(n, i);
					uint8_t color = image_get(img, front.x, front.y);
					uint8_t bgdist = bg > color ? bg - color : color - bg;
					if (bgdist > BACKGROUND_TRESHOLD) {
						image_set(img, front.x, front.y, WHITE);
						image_set(canvas, front.x, front.y, color);
						ptqueue_push(&Q, front);
					}
				}
			}

			ptqueue_free(&Q);

			image_t *letter = subimage(canvas, min.x, min.y, max.x, max.y);
			resize_height(letter, 64);
			imglist_add(list, letter);
		}
	}
	destroy_image(canvas);
	canvas = NULL;
	return list;
}

extern char* alphabet;

float compare_font(imglist letters, imglist font)
{
	float max_score = 0;
	for (unsigned int l = 0; l < letters->size; l++) {
		float lmax = 0;
		for (unsigned int i = 0; i < font->size; i++) {
			float score = compare(imglist_at(letters, l), imglist_at(font, i));
			if (score > lmax) lmax = score;
		}
		max_score += lmax;
	}
	return max_score / letters->size;
}

int main(int argc, char** argv)
{
	if (argc < 3)
	{
		fprintf(stderr, "usage: %s <image> <font>", argv[0]);
		exit(1);
	}

	char* image_name = argv[1];
	char* font_name = argv[2];

	font_init();

	imglist font = font_load(font_name);

	image_t *text = read_png(image_name);
	make_gray(text);
	imglist letters = find_letters(text);
	destroy_image(text);
	text = NULL;

	printf("Score: %f\n", compare_font(letters, font));

	imglist_destroy(&font);
	imglist_destroy(&letters);

	font_shutdown();
	return EXIT_SUCCESS;
}

