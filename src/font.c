#include "font.h"
#include "image.h"

#include <ft2build.h>
#include FT_FREETYPE_H

static FT_Library library;

void font_init()
{
	FT_Error error = FT_Init_FreeType(&library);
	if (error)
	{
		fprintf(stderr, "An error occurred while initializing FreeType: %d\n", error);
		abort();
	}
}

void font_shutdown()
{
	FT_Done_FreeType(library);
}

char *alphabet = "abcdefghijklmnopqrstuvwxyuABCDEFGHIJKLMNOPQRSTUVWXYZ";

imglist font_load(const char *file)
{
	FT_Error error;

	FT_Face face;
	error = FT_New_Face(library, file, 0, &face);
	if (error)
	{
		fprintf(stderr, "An error occurred while loading %s: %d\n", file, error);
		abort();
	}

	error = FT_Set_Pixel_Sizes(face, 0, 64);
	if (error)
	{
		fprintf(stderr, "PixelSize error: %d\n", error);
		FT_Done_Face(face);
		abort();
	}
	
	imglist list = new_imglist();
	imglist_reserve(list, strlen(alphabet));

	for (unsigned int i = 0; i < strlen(alphabet); i++) {
		unsigned int glyph_index = FT_Get_Char_Index(face, alphabet[i]);

		error = FT_Load_Glyph(face, glyph_index, FT_LOAD_DEFAULT);
		if (error)
		{
			fprintf(stderr, "LoadGlyph error: %d\n", error);
			FT_Done_Face(face);
			abort();
		}

		error = FT_Render_Glyph(face->glyph, FT_RENDER_MODE_NORMAL);
		if (error)
		{
			fprintf(stderr, "RenderGlyph error: %d\n", error);
			FT_Done_Face(face);
			abort();
		}

		FT_Bitmap *bitmap = &face->glyph->bitmap;

		image_t *img = new_image(bitmap->width, bitmap->rows);
		for (unsigned int y = 0; y < bitmap->rows; y++) {
			for (unsigned int x = 0; x < bitmap->width; x++) {
				img->data[y * bitmap->width + x] = 255 - bitmap->buffer[y * bitmap->pitch + x];
			}
		}
		resize_height(img, 64);

		imglist_add(list, img);
	}

	FT_Done_Face(face);

	return list;
}
